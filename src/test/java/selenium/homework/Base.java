package selenium.homework;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 2020-11-11 周三作业
 * 测试场景：通过cookie验证登录企业微信，并成功添加一位成员
 * */

public class Base {

    static WebDriver driver;
    static ObjectMapper mapper;

    @BeforeAll
    static void driverInit(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
    }

    @AfterAll
    static void tearDown(){
        driver.quit();
    }


    //手动登录保存cookie
    public static void writeCookie(String url) throws IOException {
        try {
            driver.get(url);
            System.out.println("***请手动扫码登录***");
            Thread.sleep(15000);
            Set<Cookie>  cookies = driver.manage().getCookies();
            mapper = new ObjectMapper(new YAMLFactory());
            mapper.writeValue(new File("cookies.yaml"),cookies);

        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    //反序列化 读取cookie文件并转化为json对象
    public static void readCookie()throws IOException{
        mapper = new ObjectMapper(new YAMLFactory());
        TypeReference <List<HashMap<String,Object>>> typeReference = new TypeReference<List<HashMap<String,Object>>>(){};
        List<HashMap<String,Object>> cookies = mapper.readValue(new File("cookies.yaml"),typeReference);
        cookies.forEach(cookieMap->{
            driver.manage().addCookie(new Cookie(cookieMap.get("name").toString(),cookieMap.get("value").toString()));
        });
        cookies.forEach(cookieMap->{
            driver.manage().addCookie(new Cookie(cookieMap.get("name").toString(),cookieMap.get("value").toString()));
        });
    }


}
