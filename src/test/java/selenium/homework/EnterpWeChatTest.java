package selenium.homework;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;


public class EnterpWeChatTest extends Base{
    static String loginUrl = "https://work.weixin.qq.com/wework_admin/loginpage_wx";
    String menuContactsLoc = "#menu_contacts";
    String addMenLoc = "//div[@class='ww_operationBar']/a[@class='qui_btn ww_btn js_add_member']";
    String userNameLoc = "username";
    String acctIdLoc = "#memberAdd_acctid";
    String mobileLoc = "#memberAdd_phone";
    String saveBtnLoc = "//form/div[1]/a[2]";

    String memNameTxt = "陈同学";
    String memIdTxt = "168test@xfzm.com";
    String memMobileTxt = "16816888888";

    WebDriverWait wait = new WebDriverWait(driver,10);



    @BeforeEach
    void browserInit() throws IOException{
//      拿到cookie,登录企业微信,如果cookie文件为空，重写cookie文件
        driver.get(loginUrl);
        try {
            readCookie();
        }catch (NullPointerException e){
            driver.quit();
            e.printStackTrace();
            writeCookie(loginUrl);
            readCookie();
        }

        driver.navigate().refresh();

    }


    @Test
    void addMemberTest() {
       //打开通讯录页面
        driver.findElement(By.cssSelector(menuContactsLoc)).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(addMenLoc))).click();

        //添加成员信息
        driver.findElement(By.id(userNameLoc)).sendKeys(memNameTxt);
        driver.findElement(By.cssSelector(acctIdLoc)).sendKeys(memIdTxt);
        driver.findElement(By.cssSelector(mobileLoc)).sendKeys(memMobileTxt);
        driver.findElement(By.xpath(saveBtnLoc)).click();

    }

}
