package selenium.preclass.testcase;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class CeshirenTest {

    private static WebDriver driver;
    private static WebDriverWait wait;

    @BeforeAll
    static void initDriver(){
        driver = new ChromeDriver();
        //隐式等待，全局
        driver.manage().timeouts().implicitlyWait(3,TimeUnit.MICROSECONDS);
        wait = new WebDriverWait(driver,10);
    }

    @Test
    @Disabled
    void login() throws InterruptedException{
        driver.get("https://ceshiren.com/");
        driver.findElement(By.xpath("//span[contains(text(),'登录')]")).click();
        driver.findElement(By.id("login-account-name")).clear();
        driver.findElement(By.id("login-account-name")).sendKeys("742312415@qq.com");
        driver.findElement(By.id("login-account-password")).sendKeys("199049Zsj");
        driver.findElement(By.xpath("//button[@id='login-button']")).click();
        Thread.sleep(1000);
    }

    @Test
    void waitTest(){
        driver.get("https://ceshiren.com/");
        driver.findElement(By.xpath("//span[contains(text(),'登录')]")).click();
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'登录')]")));
        element.click();
    }



    @AfterAll
    static void tearDown(){
        driver.quit();
    }


}
