package selenium.preclass.testcase;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;

import java.util.concurrent.TimeUnit;

class ActionsTest {

    private static WebDriver driver;
//    static WebDriverWait wait;
    private static Actions actions;


    @BeforeAll
    static void initDriver(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3,TimeUnit.MICROSECONDS);
        actions = new Actions(driver);
    }

    @Test
    void clickTest() throws InterruptedException {
        driver.get("http://sahitest.com/demo/clicks.htm");

        actions.click(driver.findElement(By.xpath("//input[@value='click me']"))).perform();
        actions.doubleClick(driver.findElement(By.xpath("//input[@value='dbl click me']"))).perform();
        actions.contextClick(driver.findElement(By.xpath("//input[@value='right click me']"))).perform();
        Thread.sleep(3000);
    }

    @Test
    void moveTest(){
        driver.get("https://www.baidu.com");
        actions.moveToElement(driver.findElement(By.id("s-usersetting-top"))).perform();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    @Test
    void dragTest(){
        driver.get("http://sahitest.com/demo/dragDropMooTools.htm");
        actions.dragAndDrop(driver.findElement(By.id("dragger")),driver.findElement(By.xpath("//*[@class='item'][last()]"))).perform();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void keyBoardTest(){
        driver.get("http://sahitest.com/demo/label.htm");
        driver.findElements(By.xpath("//input[@type=textbox]")).get(0).sendKeys("'sas");
        actions.keyDown(Keys.DOWN).sendKeys("a").keyUp(Keys.COMMAND).perform();
        actions.keyDown(driver.findElements(By.xpath("//input[@type='textbox']")).get(0),Keys.COMMAND).sendKeys().perform();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void scrollTest(){
        try {
        driver.get("https://www.baidu.com");
        driver.findElement(By.id("kw")).sendKeys("selenium");
        TouchActions touchActions = new TouchActions(driver);
        touchActions.click(driver.findElement(By.id("su")));

        JavascriptExecutor js =(JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");

        Thread.sleep(3000);

        driver.findElement(By.xpath("//a[@class='n']")).click();

        Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @AfterAll
    static void tearDown(){
        driver.quit();
    }


}
