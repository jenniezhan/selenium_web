package selenium.preclass.testcase;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

public class BaseTest {

    static WebDriver driver;

    @BeforeAll
    static void initData(){
        String browserName = System.getenv("browser");
        System.out.println("test1");

        if ("chrome".equals(browserName)){
            driver=new ChromeDriver();

        }else if ("firefox".equals(browserName)){
            System.setProperty("webdriver.gecko.driver","C:\\Program Files\\Web Drivers\\geckodriver");
            driver=new FirefoxDriver();
        }else if ("safari".equals(browserName)){
            driver = new SafariDriver();
        }

    }

    @AfterAll
    static void tearDown(){
        driver.quit();
    }
}
