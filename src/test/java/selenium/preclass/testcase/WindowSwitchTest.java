package selenium.preclass.testcase;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Set;
import java.util.concurrent.TimeUnit;

public class WindowSwitchTest {

    private static WebDriver driver;

    @BeforeAll
    static void initData() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    void switchWindowTest() {
        driver.get("https://www.baidu.com");
        driver.manage().window().maximize();
        driver.findElement(By.cssSelector(".s-top-login-btn.c-btn")).click();
        //获取当前窗口句柄
        String baiduwin = driver.getWindowHandle();
        driver.findElement(By.xpath("//a[@class='pass-reglink pass-link']")).click();
        Set<String> wins = driver.getWindowHandles();

        for (String win : wins) {
            if (!win.equals(baiduwin)){
                driver.switchTo().window(win);
                driver.findElement(By.id("TANGRAM__PSP_4__userName")).sendKeys("ashin");
                driver.findElement(By.id("TANGRAM__PSP_4__phone")).sendKeys("111111");
                driver.switchTo().window(baiduwin);
                driver.findElement(By.id("TANGRAM__PSP_11__footerULoginBtn")).click();
                driver.findElement(By.id("TANGRAM__PSP_11__userName")).sendKeys("123123");
                driver.findElement(By.id("TANGRAM__PSP_11__password")).sendKeys("1231312");

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        driver.quit();
    }


}
